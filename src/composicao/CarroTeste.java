package composicao;

public class CarroTeste {

	public static void main(String[] args) {

		Carro c1 = new Carro();
		
		System.out.println("Ligado: ->> "+ c1.estaLigado());
        
		c1.ligar();
		System.out.println("Carro Desligado: ->> " + c1.estaLigado());
		
		c1.acelerar();
		c1.acelerar();
		c1.acelerar();
		c1.acelerar();
		c1.acelerar();
		c1.acelerar();
		
		System.out.println("Acelerar" + c1.motor.giros() + " RPM s ");
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		c1.frear();
		System.out.println("");
	}

}

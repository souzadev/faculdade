package unifcv.exercicio.aula;

public class Sistemas {

	public static void main(String[] args) {
        Cliente cliente1 = new Cliente("Igor da Silva");
        Cliente cliente2 = new Cliente("Fernanda");
        
        Compra compra1 = new Compra();
        compra1.adicionarItem("Pincel", 5.05, 100);
        compra1.adicionarItem("Notebook accer", 3200.05, 1);
        compra1.adicionarItem(new Produto("Impressora", 997.02), 1);
        
        Compra compra3 = new Compra();
        compra3.adicionarItem("borracha", 5.00, 3);
        compra3.adicionarItem("borracha", 5.00, 3);
        
        Compra compra2 = new Compra();
        compra2.adicionarItem("Notebook accer", 3200.05, 2);
        compra2.adicionarItem(new Produto("Impressora", 997.02), 1);
        
        cliente1.compras.add(compra1);
        //cliente1.compras.add(compra2);
        cliente2.compras.add(compra3);        
        
        System.out.println("Compra 1: "+ cliente1.obterValorTotal());
        System.out.println("Compra 3: "+ cliente2.obterValorTotal());
	}

}
